<?php

/**
 * @file
 * Setup instructions for Price Spider.
 */
?>
<h2>Price Spider setup instructions.</h2>
<p>
  Setting up price spider is super easy. Follow the below steps.
  <ol>
    <li>Price spider generally integrates with your products displayed in the site. Before you setup this module, you need to connect with Price Spider team to get necessary information and also register all your products.</li>
    <li>Each product is supposed to have a unique SKU, as registered in Price Spider. Releated content type in drupal must have a field to store the SKU values.</li>
    <li>One the above pre-requisites are completed, please fillout the price spider settings form <a href='/admin/config/system/price_spider/settings'>here</a>.</li>
    <li>For each product page, where you want to have the price spider buttons displayed, add the following tag, <br/><strong><?php echo htmlspecialchars('<div class="ps-widget" ps-sku="[Product SKU value]"></div>', ENT_QUOTES); ?></strong> </li>
    <li>Any other pages, where you want the generic price spider button displayed, add the following tag, <br/><strong><?php echo htmlspecialchars('<div class="ps-widget" ps-sku="[Default SKU value]"></div>', ENT_QUOTES); ?></strong></li>
  </ol>


</p>
