<?php

/**
 * @file
 * Implements admin specific settings for pricespider settings module.
 */

/**
 * Implements hook_settings_form().
 */
function price_spider_settings_form($form, &$form_state) {
  $form['generic'] = array(
    '#type' => 'fieldset',
    '#title' => t('Generic Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="generic-pricespider">',
    '#suffix' => '</div>',
  );
  $form['generic']['price_spider_key'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#required' => TRUE,
    '#title' => t('Price Spider Key'),
    '#description' => t('Enter price spider key.'),
    '#default_value' => variable_get('price_spider_key', ''),
  );
  $form['generic']['price_spider_country'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#required' => TRUE,
    '#title' => t('Price Spider Country'),
    '#description' => t('Enter price spider country.'),
    '#default_value' => variable_get('price_spider_country', 'US'),
  );
  $form['generic']['price_spider_language'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#required' => TRUE,
    '#title' => t('Price Spider Language'),
    '#description' => t('Enter price spider language.'),
    '#default_value' => variable_get('price_spider_language', 'en'),
  );
  $form['generic']['price_spider_default_sku'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#required' => TRUE,
    '#title' => t('Price Spider Default SKU'),
    '#description' => t('Enter default sku of products, this would appear if no product specific sku added.'),
    '#default_value' => variable_get('price_spider_default_sku', ''),
  );
  $form['pages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="generic-pages">',
    '#suffix' => '</div>',
  );
  $form['pages']['price_spider_pages'] = array(
    '#type' => 'textarea',
    '#rows' => 5,
    '#cols' => 3,
    '#required' => FALSE,
    '#title' => t('Pages List'),
    '#description' => t('Enter pages where you want to page-spider script to appear. Enter each page in a new line.'),
    '#default_value' => variable_get('price_spider_pages', ''),
  );
  $form['pages']['price_spider_exclude_pages'] = array(
    '#type' => 'textarea',
    '#rows' => 5,
    '#cols' => 3,
    '#required' => FALSE,
    '#title' => t('Exclude Pages List'),
    '#description' => t('Enter pages where you do not want page-spider script to appear. Enter each page in a new line.'),
    '#default_value' => variable_get('price_spider_exclude_pages', ''),
  );
  $form['content_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="generic-ctypes">',
    '#suffix' => '</div>',
  );
  $types_list = [];
  $node_types = node_type_get_types();
  foreach ($node_types as $key => $node_type) {
    $types_list[$key] = $node_type->name;
  }
  $form['content_types']['price_spider_ctypes'] = array(
    '#type' => 'checkboxes',
    '#required' => FALSE,
    '#title' => t('Select Content Types'),
    '#options' => $types_list,
    '#default_value' => variable_get('price_spider_ctypes', ''),
  );
  return system_settings_form($form);
}

/**
 * Callback function to the setup instructions page.
 */
function price_spider_instruction() {
  return theme('price_spider_instruction_page', []);

}
